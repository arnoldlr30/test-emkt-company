<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Registro extends Model
{
    use SoftDeletes;

    protected $table = 'registros';
    protected $fillable = [
        'name',
        'apellido',
        'edad',
        'email',
        'habilidad',
    ];
}
