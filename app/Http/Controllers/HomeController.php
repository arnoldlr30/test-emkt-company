<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registro;

class HomeController extends Controller
{
    public function index(){

    	return view('index');
    }

    public function store(Request $request){

    if($request->id)
            $registro = Registro::findOrFail($request->id);
        else
            $registro = new Registro;
        
        $registro->fill($request->all());
        $registro->save();

        return redirect('/')->with('status', 'Registro Saved!');
    }
}
