<div class="container-md row p-5">
	<div>
		<h2>Cuéntanos sobre ti</h2>
	</div>
	<form class="flex-column col-8 p-3" method="POST" action="{{route('send')}} ">
		{{ csrf_field() }}
	  <div class="form-group p-2 col-md-10">
	    <label>Nombre</label>
	    <input type="text" name="name" class="form-control" aria-describedby="emailHelp" placeholder="Enter name">
	  </div>
	  <div class="form-group p-2 col-md-10">
	    <label>Apellido</label>
	    <input type="text" name="apellido" class="form-control" placeholder="Last name">
	  </div>
	  <div class="form-group p-2 col-md-10">
	    <label>Edad</label>
	    <input type="number" name="edad" class="form-control" placeholder="Edad">
	  </div>
	  <div class="form-group p-2 col-md-10">
	    <label>Email</label>
	    <input type="email" name="email" class="form-control" placeholder="Email">
	  </div>
		<div class="form-group p-2 col-md-10">
      	<label for="inputState">Habilidades</label>
      	<select id="inputState" name="habilidad" class="form-control">
        <option selected>Choose...</option>
        	<option value="PHP">PHP</option>
	        <option value="HTML">HTML</option>
	        <option value="CSS">CSS</option>
	        <option value="Laravel">Laravel</option>
	        <option value="Java">Java</option>
	        <option value="Javascript">Javascript</option>
	        <option value="Angular">Angular</option>
	        <option value="Node">Node</option>
	        <option value="Linux">Linux</option>
	        <option value="Servidores">Servidores</option>
      </select>
    </div>
	  <button type="submit" class="btn btn-primary">Submit</button>
	  @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
	</form>
	<div class="col-4">
		<img src="img/paper-plane.svg" style="height: 75%;">
	</div>
</div>
