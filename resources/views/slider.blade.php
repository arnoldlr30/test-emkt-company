<section>
<div class="container d-flex justify-content-around p-2">
	<div class="d-flex flex-column mt-5">
		<h1 class="text-dark">EMKT Company</h1>
		<h4 class="">Empresa de desarrollo de software en El Salvador</h4>
	</div>
	<div class="ml-3">
		<img class="p-2" src="img/slider.jpg" style="height: 400px; width: 400px;">
	</div>
</div>
<div class="d-flex justify-content-around bg-light p-5">
	<div class="card" style="width: 18rem;">
  <img class="card-img-top p-5" src="img/code.svg" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Web Development</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="#" class="btn btn-primary">Go somewhere</a>
  </div>
</div>
<div class="card" style="width: 18rem;">
  <img class="card-img-top p-5" src="img/design.svg" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Web Design</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="#" class="btn btn-primary">Go somewhere</a>
  </div>
</div>
<div class="card" style="width: 18rem;">
  <img class="card-img-top p-5" src="img/development.svg" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">Software developer</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
    <a href="#" class="btn btn-primary">Go somewhere</a>
  </div>
</div>
</div>
</section>