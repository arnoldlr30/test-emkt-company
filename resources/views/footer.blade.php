<style type="text/css">
	i{
		font-size: 32px;
		padding: 10px;
	}
</style>
<div class="bg-dark p-5">
	<p class="text-white">This is the footer</p>
	<div class="social-media">
		<a href="" target="_blank"><i class="text-white fab fa-facebook"></i></a>	
		<a href="" target="_blank"><i class="text-white fab fa-instagram"></i></a>
		<a href="" target="_blank"><i class="text-white fab fa-whatsapp"></i></a>
		<a href="" target="_blank"><i class="text-white fab fa-twitter"></i></a>
		<a href="mailto:arnoldlr2018@gmail.com"><i class="text-white fas fa-envelope"></i></a>	
		</div>
</div>